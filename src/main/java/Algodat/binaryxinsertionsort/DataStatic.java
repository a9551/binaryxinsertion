package Algodat.binaryxinsertionsort;
import java.util.Arrays;
import java.util.Scanner;

public class DataStatic {
        public static void main(String[] args) {
            int[] nilai = {2, 4, 3, 6, 9, 12, 45, 76, 87, 23};
            Scanner scan = new Scanner(System.in);

            System.out.println("=== Insertion Sort");
            System.out.println("=== Data Sebelum Diurutkan");
            System.out.println(Arrays.toString(nilai));
            System.out.println("=== Data Setelah Diurutkan");
            System.out.println(Arrays.toString(InsertionSort(nilai)));

            System.out.println();
            System.out.println("=== Binary Search");
            System.out.print("=== Masukkan angka yang diinginkan: ");
            int cari = scan.nextInt();
            int hasil= binarySearch(nilai, cari, 0, nilai.length);
                    if (hasil >=0){
                        System.out.println("=== Angka "+ cari +" di temukan di index = " + hasil);
                    }
                    else{
                        System.out.println("=== Angka tidak ditemukan");
                    }
                    scan.close();
                }
                private static int[] InsertionSort(int[] angka) {
                    for (int i = 1; i < angka.length; i++) {
                        for (int j = i; j > 0; j--) {
                            if (angka[j] < angka[j-1]) {
                                int temp = angka[j];
                                angka[j] = angka[j-1];
                                angka[j-1] = temp;
                            }
                        }
                    }
                    return angka;
                }
                static int binarySearch(int[] angka, int cari, int i, int j){
                    int tengah;
                    while (i<=j) {
                        tengah = (i+j)/2;
                        if (cari == angka[tengah])return tengah;
                        else if(cari<angka[tengah])j = tengah-1;
                        else i = tengah+1;
                    }
                    return -1;
        }

}
